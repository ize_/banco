package banco;

import java.util.Scanner;
import javax.swing.JOptionPane;
public class Banco {

    public static void main(String[] args) {   
    String op="";//se crea una variable que recibira la opcion 
    Cuenta cuenta1 = new Cuenta(50);
        do{//comienzo del ciclo
            op=JOptionPane.showInputDialog("a. Ingreso de datos\nb. Abono a cuenta\nc. Retiro de cuenta\nd. Consulta de saldo\ne. Salir.");
            //se usa un cuadro de dialogo de tipo entrada el cual recibira la opcion
 
            if(op.equals("a")){//si se elige la opcion a se muestra el siguiente mensaje
                //Ingreso de datos        
                String nom,edad,correo;
                nom=JOptionPane.showInputDialog("Cual es tu nombre:");
                edad=JOptionPane.showInputDialog("Cual es tu edad:");
                correo=JOptionPane.showInputDialog("Cual es tu correo:");
                String mensajeDatos = String.format("Hola "+nom+" tienes la edad "+edad+" tu correo es "+correo);
                JOptionPane.showMessageDialog(null,mensajeDatos);
            }
            else if(op.equals("b")){//si se elige la opcion b se muestra el siguiente mensaje
                //Deposita el monto
                double montoDeposito;
                //Inicia secuencia para el deposito de cuenta uno
                montoDeposito = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el monto a depositar:"));
                String mensajeAbono = String.format("Depositando %.2f saldo a la cuenta",montoDeposito);
                JOptionPane.showMessageDialog(null,mensajeAbono);
                cuenta1.abonar(montoDeposito);
            }
            else if(op.equals("c")){//si se elige la opcion c se muestra el siguiente mensaje
                //Retiro el monto
                double montoDeposito;
                //Inicia secuencia para el deposito de cuenta uno
                montoDeposito = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el monto a retirar:"));
                String mensajeRetiro = String.format("Retirando de %.2f la cuenta",montoDeposito);
                JOptionPane.showMessageDialog(null,mensajeRetiro);
                cuenta1.retiro(montoDeposito);
            }
            else if(op.equals("d")){//si se elige la opcion d se cierra o finaliza la aplicacion
                //Mostrar el saldo de cada objeto
                String mensaje1 = String.format("Saldo de cuenta: $%.2f\n",cuenta1.obtenerSaldo());
                JOptionPane.showMessageDialog(null,mensaje1);
            }
            else if(op.equals("e")){//si se elige la opcion d se cierra o finaliza la aplicacion
                System.exit(0);//comando para cerrar las aplicaciones
            }
            else{//si la opcion ingresada es diferente a las mostradas se muestra el siguiente mensaje
                JOptionPane.showMessageDialog(null, "Ingrese una opcion valida");
            }
        }while(!op.equals("e"));//finaliza el ciclo con la condicional que hay dentro del while

        
      
    }
    
}
